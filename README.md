# OpenML dataset: Meta_Album_PNU_Micro

https://www.openml.org/d/44312

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album PanNuke Dataset (Micro)**
***
The PanNuke dataset(https://jgamper.github.io/PanNukeDataset/) is a semi-automatically generated segmentation and classification task of nuclei. The dataset contains 7 753 images of 19 different tissue types. For the Meta-Album meta-dataset, even though this dataset was designed as a segmentation task, we were able to transform it into a tissue classification task since we had the tissue type for each sample in the dataset. We also resized the images to 128x128 pixels and applied stain normalization to avoid bias and remove some spurious features.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/PNU.png)

**Meta Album ID**: MCR.PNU  
**Meta Album URL**: [https://meta-album.github.io/datasets/PNU.html](https://meta-album.github.io/datasets/PNU.html)  
**Domain ID**: MCR  
**Domain Name**: Microscopic  
**Dataset ID**: PNU  
**Dataset Name**: PanNuke  
**Short Description**: 19 Human Tissues Dataset  
**\# Classes**: 20  
**\# Images**: 800  
**Keywords**: microscopic, human tissues  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Attribution-NonCommercial-ShareAlike 4.0 International  
**License URL(original data release)**: https://warwick.ac.uk/fac/cross_fac/tia/data/pannuke
https://creativecommons.org/licenses/by-nc-sa/4.0/
 
**License (Meta-Album data release)**: Attribution-NonCommercial-ShareAlike 4.0 International  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/)  

**Source**: PanNuke: An Open Pan-Cancer Histology Dataset for Nuclei Instance Segmentation and Classification  
**Source URL**: https://jgamper.github.io/PanNukeDataset/  
  
**Original Author**: Gamper, Jevgenij and Koohbanani, Navid Alemi and Benet, Ksenija and Khuram, Ali and Rajpoot, Nasir  
**Original contact**: j.gamper@warwick.ac.uk  

**Meta Album author**: Romain Mussard  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{gamper2019pannuke,
  title={PanNuke: an open pan-cancer histology dataset for nuclei instance segmentation and classification},
  author={Gamper, Jevgenij and Koohbanani, Navid Alemi and Benet, Ksenija and Khuram, Ali and Rajpoot, Nasir},
  booktitle={European Congress on Digital Pathology},
  pages={11--19},
  year={2019},
  organization={Springer}
}

@article{gamper2020pannuke,
  title={PanNuke Dataset Extension, Insights and Baselines},
  author={Gamper, Jevgenij and Koohbanani, Navid Alemi and Graham, Simon and Jahanifar, Mostafa and Khurram, Syed Ali and Azam, Ayesha and Hewitt, Katherine and Rajpoot, Nasir},
  journal={arXiv preprint arXiv:2003.10778},
  year={2020}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Mini]](https://www.openml.org/d/44297)  [[Extended]](https://www.openml.org/d/44330)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44312) of an [OpenML dataset](https://www.openml.org/d/44312). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44312/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44312/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44312/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

